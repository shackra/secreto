;;; secreto.el --- Hide away parts of your configuration

;; Copyright (C) 2016-2019 Jorge Araya Navarro

;; Author: Jorge Javier Araya Navarro <jorgejavieran@yahoo.com.mx>
;; Version: 0.2
;; Package-Requires: ((emacs "24"))
;; Keywords: convenience
;; URL: https://gitlab.com/shackra/secreto

;; This file is not part of GNU Emacs.

;;; Commentary:

;; This package provides a way to kept secret anything that should kept secret,
;; making you more comfortable when publishing your Emacs configuration
;; files.  This package was inspired by senstive.el

;;; Usage:

;; Just put any elisp file under the corresponding `secreto-root' directory
;; with any content you want/need.

;;; Code:

(defgroup secreto '()
  "Customization group for `secreto`"
  :group 'convenience
  :link '(url-link "https://gitlab.com/shackra/secreto"))

(defcustom secreto-root "~/.secret"
  "Root directory where secret elisp code lives."
  :type  'directory
  :group 'secreto)

(defun secreto-load-file (file)
  "Insert a FILE content into  a temporal buffer and evaluate its content."
  (with-temp-buffer
    (insert-file-contents file nil nil nil t)
    (eval-buffer)))

;;;###autoload
(defun secreto-load ()
  "Load all emacs-lisp files under path SECRETO-ROOT."
  (if (file-directory-p secreto-root)
      (mapc 'secreto-load-file (directory-files secreto-root t ".el$"))
    (error "Directory pointed by `secreto-root` (%s) does not exist or is not a directory" secreto-root)))

(provide 'secreto)
;;; secreto.el ends here
